﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Adelante : MonoBehaviour {
    float y=0.0f;
    public float velocidad=1f;
    public float alturaMax=50f;
    public float alturaMin=-0f;
    bool direccion=true;
    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update () {
        if (direccion == true) {
            transform.Translate (y,0,0);
            y = +velocidad;
            if (transform.position.y >= alturaMax) {
                direccion = false;
            }
        }
        if (direccion == false) {
            transform.Translate (0,-y,0);
            y = -velocidad;
            if (transform.position.y <=alturaMin) {
                direccion = true;
            }
        }

        //if (y>15)5.95  -3.8
        //transform.Translate (0,y, 0);
        //y = -velocidad;

    }
}